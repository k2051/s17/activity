let students=[]

function addStudent(i){
	students.push(i)
	console.log(i + ' has been added to student list')
}



function countStudents(){
	console.log('There are a total of '+students.length+' students')
}
// countStudents()

function printStudents(){
	students.sort(
			function(a,b){
				return a-b
			}
		)
	students.forEach(
			function(i){
				console.log(i)
			}
		)
}

// printStudents()

function findStudent(b){
	let searchStudent=students.filter(
			function(i){
				return i.toLowerCase().includes(b)
			}
		)
	if (searchStudent.length>=2) {
		studentNames=searchStudent.join()
		console.log(studentNames+ " are enrollees")
	} else if (searchStudent.length==1){
		studentName=searchStudent.join()
		console.log(studentName+ " is an enrollee")
	} else {
		console.log('No student found with the name '+ b)
	}
	
}


// Stretch Goals
function addSection(c){
	let mapStudents=students.map(
			function(e){
				return `${e} - Section ` + c
			}
		)
	console.log(mapStudents)
}


function removeStudent(d){
	let capt=d.toUpperCase().slice(0,1)+ d.slice(1,d.length)
	stud=students.splice(students.indexOf(capt),1)
	console.log(stud+' has been removed from the list')
	// console.log(students)
}

